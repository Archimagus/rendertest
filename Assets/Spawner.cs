﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Spawner : MonoBehaviour
{
	[SerializeField]GameObject _prefab;
	[SerializeField] int _count;
	[SerializeField] float _radius;
	[SerializeField] Shader _pickShader;
	MeshFilter _meshFilter;
	Camera _camera;
	int _pickIndex = 0;

	Color32[] _colors;
	RenderTexture _rt;
	Texture2D _pt;

	[ContextMenu("spawn")]
	void Start ()
	{
		_camera = Camera.main;
		_meshFilter = GetComponent<MeshFilter>();
		var verts = new Vector3[_count];
		_colors = new Color32[_count];
		var indices = new int[_count];
		var pickColors = new Vector4[_count];

		for (int i = 0; i < _count; i++)
		{
			verts[i] = UnityEngine.Random.insideUnitSphere * _radius;
			_colors[i] = Color.white;
			indices[i] = i;

			// create a Take our index and pack it into a vector4 that will be treaded as a color by the picking shader
			var bytes = BitConverter.GetBytes(i);
			var pc = new Vector4(bytes[0]/255f, bytes[1] / 255f, bytes[2] / 255f, bytes[3] / 255f);
			pickColors[i] = pc;
		}
		Mesh m = new Mesh();
		m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
		m.name = "Points";
		m.vertices = verts;
		m.colors32 = _colors;
		m.SetUVs(0, pickColors.ToList());
		m.SetIndices(indices, MeshTopology.Points, 0);
		_meshFilter.mesh = m;
	}

	
	
	void Update ()
	{

		var h = Input.GetAxis("Horizontal");
		var v = Input.GetAxis("Vertical");

		transform.Rotate(v, h, 0f);

		if (Input.GetMouseButtonDown(0))
		{
			doPicking();
		}
	}

	private void doPicking()
	{
		if (_rt == null || _rt.width != Screen.width || _rt.height != Screen.height)
			createPickingTextures();

		var cf = _camera.clearFlags;
		var cc = _camera.backgroundColor;

		// Make sure the camera is going to clear to -1
		_camera.clearFlags = CameraClearFlags.SolidColor;
		var bytes = BitConverter.GetBytes(-1);
		_camera.backgroundColor = new Color32(bytes[0], bytes[1], bytes[2], bytes[3]);
		// Render to a render texture using a a special picking shader
		_camera.targetTexture = _rt;
		_camera.RenderWithShader(_pickShader, string.Empty);
		RenderTexture.active = _rt;
		// Read the render texture into a Texture2D
		_pt.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		// Reset everything back to normal
		RenderTexture.active = null;
		_camera.targetTexture = null;
		_camera.backgroundColor = cc;
		_camera.clearFlags = cf;

		// Get the texture colors as an array
		var colors = _pt.GetPixels32();

		// Get the color at the mouse position
		var mousePos = Input.mousePosition;
		var c = colors[(int)(mousePos.y * Screen.width + mousePos.x)];

		// Convert that to an index
		int pickIndex = BitConverter.ToInt32(new[] { c.r, c.g, c.b, c.a }, 0);

		// -1 is the camera clear color
		if (pickIndex >= 0)
		{
			// set the old picked point to white
			_colors[_pickIndex] = Color.white;
			_pickIndex = pickIndex;
			// set the new picked point to red
			_colors[_pickIndex] = Color.red;

			// set the colors array back to the mesh so we render the newly picked point
			var m = _meshFilter.mesh;
			m.colors32 = _colors;
		}
	}

	private void createPickingTextures()
	{
		if(_rt != null)
		{
			DestroyImmediate(_rt);
			DestroyImmediate(_pt);
		}

		_rt = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.ARGBFloat);
		_pt = new Texture2D(Screen.width, Screen.height, TextureFormat.RGBAFloat, false, true);
	}
}

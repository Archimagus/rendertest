﻿Shader "Unlit/PointParticlePicking"
{
	Properties
	{
		_Size ("Size", Float) = 1
	}
	SubShader
	{
		Tags{ "Queue" = "Geometry" "IgnoreProjector" = "True" "RenderType" = "Opaque" "PreviewType" = "Plane" }
		Cull Off Lighting Off ZWrite On

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float3 position : POSITION;
				float4 pickColor : TEXCOORD0;
			};

			struct v2g
			{
				float4 position : POSITION;
				float4 pickColor : TEXCOORD0;
			};

			struct g2f
			{
				float4 position : SV_POSITION;
				float4 color : COLOR;
			};
			
			v2g vert (appdata v)
			{
				v2g o;
				o.position = UnityObjectToClipPos(v.position);
				o.pickColor = v.pickColor;
				return o;
			}

			float _Size;

			[maxvertexcount(4)]
			void geom(point v2g p[1], inout TriangleStream<g2f> stream)
			{
				g2f output = (g2f)0;
				output.color = p[0].pickColor;

				float aspect = _ScreenParams.y / _ScreenParams.x;

				float x = _Size * aspect;
				float y = _Size;

				output.position = p[0].position + float4(x, -y, 0, 0);
				stream.Append(output);
				output.position = p[0].position + float4(-x, -y, 0, 0);
				stream.Append(output);
				output.position = p[0].position + float4(x, y, 0, 0);
				stream.Append(output);
				output.position = p[0].position + float4(-x, y, 0, 0);
				stream.Append(output);
			}
			
			float4 frag (g2f i) : SV_Target
			{
				return  i.color;
			}
			ENDCG
		}
	}
}

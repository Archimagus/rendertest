﻿Shader "Unlit/PointParticle"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Size ("Size", Float) = 1
	}
		SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
		Blend SrcAlpha One
		Cull Off Lighting Off ZWrite Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 position : POSITION;
				float4 color : COLOR;
			};

			struct v2g
			{
				float4 position : POSITION;
				float4 color : COLOR;
			};

			struct g2f
			{
				float2 uv : TEXCOORD0;
				float4 position : SV_POSITION;
				float4 color : COLOR;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2g vert (appdata v)
			{
				v2g o;
				o.position = UnityObjectToClipPos(v.position.xyz);
				o.color = v.color;
				return o;
			}

			float _Size;

			[maxvertexcount(4)]
			void geom(point v2g p[1], inout TriangleStream<g2f> stream)
			{
				g2f output = (g2f)0;
				output.color = p[0].color;

				float aspect = _ScreenParams.y / _ScreenParams.x;

				float x = _Size * aspect;
				float y = _Size;

				output.position = p[0].position + float4(x, -y, 0, 0);
				output.uv = float2(1.0f, 1.0f);
				stream.Append(output);
				output.position = p[0].position + float4(-x, -y, 0, 0);
				output.uv = float2(0.0f, 1.0f);
				stream.Append(output);
				output.position = p[0].position + float4(x, y, 0, 0);
				output.uv = float2(1.0f, 0.0f);
				stream.Append(output);
				output.position = p[0].position + float4(-x, y, 0, 0);
				output.uv = float2(0.0f, 0.0f);
				stream.Append(output);
			}
			
			fixed4 frag (g2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				return  i.color*col;
			}
			ENDCG
		}
	}
}
